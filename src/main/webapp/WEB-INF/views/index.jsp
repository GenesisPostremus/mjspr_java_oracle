<%--
  Created by IntelliJ IDEA.
  User: Thibault
  Date: 20/05/2020
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  	<meta charset="UTF-8">
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>RECYCL - Demandes</title>
  </head>
  <body>
  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <a class="navbar-brand text-success" href="<c:url value="/"/>">RECYCL</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav">
	      <li class="nav-item active">
	        <a class="nav-link" href="<c:url value="/"/>">Demandes</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="<c:url value="/donnees"/>">Données</a>
	      </li>
	    </ul>
	  </div>
	</nav>
  
  	<div class="m-2">
	  	<div class="form-group row">
		  <label for="example-date-input" class="col-3 col-form-label">Demandes faites après le :</label>
		  <div class="col-3">
		    <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
		  </div>
		</div>
		
		<div id="accordion1">
			<p>
			  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#toutesDemandes" aria-expanded="false" aria-controls="collapseExample">
			    Toutes les demandes
			  </button>
			  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#nonInscrites" aria-expanded="false" aria-controls="collapseExample">
			    Demandes non inscrites
			  </button>
			</p>
			
			<div class="collapse m-4" id="toutesDemandes" data-parent="#accordion1">
			  <div class="card card-body">
			  	<div class="row" id="accordion2">
			  		<div class="col-6">
						<ul class="list-group">
						  <li class="list-group-item d-flex justify-content-between align-items-center">
						    Energym
						    <button class="btn btn-primary badge" type="button" data-toggle="collapse" data-target="#description1" aria-expanded="false" aria-controls="collapseExample">Voir plus</button>
						  </li>
						  <li class="list-group-item d-flex justify-content-between align-items-center">
						    La Clé des Champs
						    <button class="btn btn-primary badge">Voir plus</button>
						  </li>
						  <li class="list-group-item d-flex justify-content-between align-items-center">
						    Formalys
						    <button class="btn btn-primary badge">Voir plus</button>
						  </li>
						  <li class="list-group-item d-flex justify-content-between align-items-center">
						    Cartoooche
						    <button class="btn btn-primary badge">Voir plus</button>
						  </li>
						  <li class="list-group-item d-flex justify-content-between align-items-center">
						    Propack
						    <button class="btn btn-primary badge">Voir plus</button>
						  </li>
						</ul>
					</div>
				
					<div class="collapse col-6" id="description1" data-parent="#accordion2">
						<div class="card">
						  <div class="card-body">
						    <h5 class="card-title">Energym</h5>
						    <p class="card-text">
							    <p>
							    	10 Boulevard de Pontoise <br>
							    	95000 Pontoise
							    </p>
							    <p>
							    	Tournée du : 10 / 07 / 2020
							    </p>
							    
							    <table class="table mt-3">
								  <thead>
								    <tr>
								      <th scope="col">Type de déchet</th>
								      <th scope="col">Quantité à récupérée</th>
								    </tr>
								  </thead>
								  <tbody>
								    <tr>
								      <td>Alimentaire</td>
								      <td>20</td>
								    </tr>
								    <tr>
								      <td>Papier</td>
								      <td>15</td>
								    </tr>
								    <tr>
								      <td>Ordures ménagères</td>
								      <td>15</td>
								    </tr>
								    <tr>
								      <td>Verre</td>
								      <td>18</td>
								    </tr>
								  </tbody>
								</table>
						    </p>
						  </div>
						</div>
					</div>
			  	</div>
			  </div>
			</div>
			
			<div class="collapse m-4" id="nonInscrites" data-parent="#accordion1">
			  <div class="card card-body row">
			    <ul class="list-group col-6">
				  <li class="list-group-item d-flex justify-content-between align-items-center">
				    Perenco
				    <button class="btn btn-primary badge">Voir plus</button>
				  </li>
				  <li class="list-group-item d-flex justify-content-between align-items-center">
				    Ciambelli
				    <button class="btn btn-primary badge">Voir plus</button>
				  </li>
				  <li class="list-group-item d-flex justify-content-between align-items-center">
				    AJLC
				    <button class="btn btn-primary badge">Voir plus</button>
				  </li>
				  <li class="list-group-item d-flex justify-content-between align-items-center">
				    Antada
				    <button class="btn btn-primary badge">Voir plus</button>
				  </li>
				  <li class="list-group-item d-flex justify-content-between align-items-center">
				    Mavick
				    <button class="btn btn-primary badge">Voir plus</button>
				  </li>
				</ul>
			  </div>
			</div>
		</div>
 	</div> 
  
  </body>
</html>