<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
  	<meta charset="UTF-8">
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>RECYCL - Donnees</title>
  </head>
  <body>
  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <a class="navbar-brand text-success" href="<c:url value="/"/>">RECYCL</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <a class="nav-link" href="<c:url value="/"/>">Demandes</a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="<c:url value="/donnees"/>">Données</a>
	      </li>
	    </ul>
	  </div>
	</nav>
  
  	<div class="m-2">
		<div id="accordion">
			<p>
			  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#quantite" aria-expanded="false" aria-controls="collapseExample">
			    Quantité de déchet récupérée
			  </button>
			  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#employe" aria-expanded="false" aria-controls="collapseExample">
			    Employés et tournées
			  </button>
			  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#entreprise" aria-expanded="false" aria-controls="collapseExample">
			    Entreprise et demandes
			  </button>
			</p>
			
			<div class="collapse" id="quantite" data-parent="#accordion">
			  <div class="card card-body">
			    Quantité de déchet récupérée
			  </div>
			</div>
			
			<div class="collapse" id="employe" data-parent="#accordion">
			  <div class="card card-body">
			  	Employés et tournées
			  </div>
			</div>
			
			<div class="collapse" id="entreprise" data-parent="#accordion">
			  <div class="card card-body">
			  	Entreprise et demandes
			  </div>
			</div>
		</div>
 	</div> 
  
  </body>
</html>