package fr.epsi.b3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DonneesController {
	
	@GetMapping({"/donnees"})
    public String getDonnees(Model model) {
        return "donnees";
    }

}
