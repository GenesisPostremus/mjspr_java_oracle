package fr.epsi.b3.controller;

import fr.epsi.b3.modele.Demande;
import fr.epsi.b3.service.DemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@Scope("session")
public class IndexController {

    @Autowired
    private DemandeService demandeService;

    @GetMapping({"/"})
    public String getIndex(Model model) {
        return "index";
    }

    @GetMapping(path = "/{demande_id}")
    public String getDemande(@PathVariable String demande_id, Model model) {
        Demande demande = demandeService.getDemandeFromId(demande_id);
        model.addAttribute("demande", demande);
        return "index";
    }

}
