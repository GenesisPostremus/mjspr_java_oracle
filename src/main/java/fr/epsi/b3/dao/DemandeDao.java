package fr.epsi.b3.dao;

import fr.epsi.b3.modele.Demande;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class DemandeDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Demande getDemandeFromId(String id) {
        return entityManager.createQuery("select d from demande d where u.id = :id", Demande.class).setParameter("id", id).getSingleResult();
        //return entityManager.find(Demande.class, Long.getLong(id));
    }
}
