package fr.epsi.b3.modele;

import javax.persistence.*;
import java.util.Set;

@Table(name = "type_de_dechet")
public class TypeDeDechet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_dangerosite")
    private Dangerosite dangerosite;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "demande_type_de_dechet", joinColumns = @JoinColumn(name = "fk_type_de_dechet", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "fk_demande", referencedColumnName = "id"))
    private Set<Demande> demandes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Demande> getDemandes() {
        return demandes;
    }

    public void setDemandes(Set<Demande> demandes) {
        this.demandes = demandes;
    }
}
