package fr.epsi.b3.modele;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;

@Table(name = "dangerosite")
public class Dangerosite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotNull
    @Column(name = "niveau", nullable = false)
    private String niveau;

    @OneToMany(mappedBy = "dangerosite", fetch = FetchType.EAGER)
    private List<TypeDeDechet> typeDeDechets;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public List<TypeDeDechet> getTypeDeDechets() {
        return typeDeDechets;
    }

    public void setTypeDeDechets(List<TypeDeDechet> typeDeDechets) {
        this.typeDeDechets = typeDeDechets;
    }
}
