package fr.epsi.b3.service;

import fr.epsi.b3.dao.DemandeDao;
import fr.epsi.b3.modele.Demande;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DemandeService {

    @Autowired
    private DemandeDao demandeDao;

    @Transactional
    public Demande getDemandeFromId(String id){
        return demandeDao.getDemandeFromId(id);
    }

}
